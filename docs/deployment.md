# Publishing the website - deployment to production

In order to deploy the project it must be built in production mode (see the command below). After the build succedes all distributable files can be found inside the `/dist` folder. You just need to copy them into the root folder of your hosting provider.

## Deployment instructions

1. Run `npm run build` - This builds the app in production mode. No server will start just compiling and asset bundling.
2. Copy the content of the `/dist` folder into the appropriate directory of the server you want to host your site.
