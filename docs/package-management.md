# Using 3rd party libraries - package management with npm

When you use the webpack-sitebuild-kickstarter it is recommended to use `npm`  - the node package manager - to manage 3rd party library dependencies. 

## Adding new dependencies

To add a new dependecy needed by your project simply run the `npm install {package_name}` command as usual. 

## Removing dependencies

To remove a package you just experimented with, just run the `npm uninstall {package_name}` command. 

For further reading about npm commands please refer to the official [npm CLI documentation](https://docs.npmjs.com/cli-documentation/).
