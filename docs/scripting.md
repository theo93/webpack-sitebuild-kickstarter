# Organising JavaScript files - the import statement

To keep the build process very simple there is no TypeScript support by default. Thus the entry point for JavaScript files is the `src/js/main.js` file. All external modules should be imported in this file using ES2015 `import` syntax.

## Syntax examples

Example to import JQuery:

`import $ from 'jquery';`
