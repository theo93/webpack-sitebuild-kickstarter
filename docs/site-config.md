# Site configuration - frequently customised webpack configs extracted out

The `site.config.json` file  used to declare settings which may vary between applications, but covers such a common purpose worth holding inside a configuration file.

Settings path | Type | Description | Example
-------------|------|-------------|--------
`title` | string | The title of the website. Commonly used as the value of the `<title>` tag in the template as a binding. See [the website layout](./site-layout.md) for more detail of template bindings.|`{ "title": "My wonderful application" }`