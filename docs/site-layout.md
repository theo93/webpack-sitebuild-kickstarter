# The website layout - index.html

Because of the [HtmlWebpackPlugin](https://github.com/jantimon/html-webpack-plugin) some basic template bindings can be used inside of the site layout built in `index.html`.

They should be written in lodash syntax and all variables are referenced on the `htmlWebpackPlugin.options` object like `<%= htmlWebpackPlugin.options.property_name %>` 

## Supported template variables

Property name | Description | Example
-- | -- | --
`appVersion` | Replaced with the application version from the `package.json` file. | `<%= htmlWebpackPlugin.options.appVersion %>` // Output:  1.0.0
`title` | The title option from the `site.config.json` file. | `<%= htmlWebpackPlugin.options.title %>` // Output: My wonderful application