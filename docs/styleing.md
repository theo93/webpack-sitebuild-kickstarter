# Organising CSS files - SCSS/SASS

A pre-configured sass-loader is included in the webpack config, so the styles of the site can be written in the SCSS syntax. (Regular CSS can also be used as SCSS is an extension of it.) Please refer to the [sass docs](https://sass-lang.com/) for more details.

The entry point for the CSS files is the `src/style/main.scss` file. Any other CSS (SCSS) files should be `@import`'ed there.

## Splitting the code into multiple files

CSS splitting can be achieved with regular `@import` CSS import statements. Any other SCSS powered technique can also used. Please refer to the [sass docs](https://sass-lang.com/) for more details.

## Using CSS of external libraries

CSS of external libraries (3rd party packages) should be imported here using the `@import` statement. Example to import bootstrap you can write the following at the top of the `src/style/main.scss` file:

`@import '~bootstrap/dist/css/bootstrap.min.css';`