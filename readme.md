# Project Readme - webpack-sitebuild-kickstarter

The main goal of this project is to provide a simple, convenient way to develop framework-less (no Angular, AngularJS, React, etc.) one page websites using webpack.

## Documentation
  * [The website layout - index.html](./docs/site-layout.md)
  * [Organising CSS files - SCSS/SASS](./docs/styleing.md)
  * [Organising JavaScript files - the import statement](./docs/scripting.md)
  * [Using 3rd party libraries - package management with npm](./docs/package-management.md)
  * [Site configuration - frequently customised webpack configs extracted out](./docs/site-config.md)
  * [Publishing the website - deployment to production](./docs/deployment.md)

## Getting Started

_Want to jump straight into development?_

See the `1. Installing dependencies` and then the `2.a Building for development` section!

_Have not read the about the project structure yet?_

Then it is highly recommended to read the next section first! It is very important to know because the build process depends on it!

## Project structure overview

This section describes the folder structure in which you are expected to organise your source files. This is recommended since the build process depends on it!

The '`/`' refers to the project root. (The folder this readme file is in.)

- `/src/img` - Folder for custom images and icons (.png, .jpg, .gif, .svg extensions supported out of the box).
  
   __Avoid__ creating a `/lib` subfolder here as the __build process uses that__ folder to place 3rd party images in!
- `/src/js` - Folder for custom _JavaScript_ files. __Non 3rd party__ files __should not__ be placed here! (No typescript support out-of-the box.)

  Entry point: `/src/js/main.js`

- `/src/style` - Folder for custom _CSS_ files. (SASS preprocessor pre-configured and recommended to use)   
  
  Entry point: `/src/style/main.scss`
- `/src/index.html` - The only HTML file of the site.

  Contains the layout, site title and other common HTML stuff. (meta tags, etc.)

  __Note:__ \<script\>, \<link\> and \<style\> tags should be avoided. It is recommended to `import` them in the apropriate place (`main.js` or `main.scss`).

- `/node_modules` - Place for 3rd party libraries. Installed with `npm`, you are not expected to put or change anything here.

## Building the project

The 3rd party libraries are managed with npm packages so they are kept inside the `/node_modules` folder. The following two sections will walk through how to install the required dependencies and build the project for either development or production.

### 1. Installing dependencies

1. Run the `npm install` command to install the node packages. Dependencies should be added to `package.json` first.
2. Execute the command in _2.a_ to build for development or the commaind in _2.b_ to build for production.

### 2.a Building for development

Build command: `npm start`

Builds the project and launches the webpack live development server. 

__Should not be used in production!__

### 2.b Building for production

Build command: `npm run build` 

Builds the project in prodution mode, places the binaries to the `/dist` folder.

It bundles up everything needed by the production site to the apropriate place. Minifies JS, concatenates 3rd party libraries etc. 

