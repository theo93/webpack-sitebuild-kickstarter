const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const packageJson = require('./package.json');
const siteConfigJson = require('./site.config.json');

module.exports = {
  entry: {
    main: './src/js/main.js',
    styles: './src/style/main.scss'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new CopyWebpackPlugin([
      { from: path.resolve(__dirname, 'src/img'), to: path.resolve(__dirname, 'dist/img') }
      ]),
      new MiniCssExtractPlugin({
        filename: "style.css",
      }),
      new HtmlWebpackPlugin({
        title: siteConfigJson.title,
        appVersion: packageJson.version,
        template: path.resolve(__dirname, 'src/index.html'),
        excludeAssets: [/styles.*.js/] // exclude style.js or style.[chunkhash].js
      }),
      new HtmlWebpackExcludeAssetsPlugin(),
      new FixStyleOnlyEntriesPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'img/lib/[name]-[hash].[ext]?[hash]'
            }
          }
        ]
      }      
    ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },  
};